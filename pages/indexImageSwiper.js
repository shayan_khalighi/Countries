import Head from 'next/head'
import ImageSwiper from '../components/ImageSwiper'


export default function Home() {
    
  return (
    <div >
    
    <Head>Countries</Head>
      <ImageSwiper />
    </div>
  )
}

