
import style from './countrySwiper.module.css'
import React from 'react'
import axios from 'axios'
import {Swiper, SwiperSlide} from "swiper/react";
import Zoomable from "react-instagram-zoom";


import "swiper/swiper.min.css";

export default class ImageSwiper extends React.Component {
   
    
      render(){
          
    return (
        <div className={style.container}>
        <h1>Countries</h1>
        <Swiper>
        
        
        <SwiperSlide><Zoomable ><img src="https://source.unsplash.com/random/200x200?sig=1"></img></Zoomable></SwiperSlide> 
        <SwiperSlide><Zoomable><img src="https://source.unsplash.com/random/200x200?sig=2"></img></Zoomable></SwiperSlide> 
         <SwiperSlide><Zoomable><img src="https://source.unsplash.com/random/200x200?sig=3"></img></Zoomable></SwiperSlide> 
        <SwiperSlide><Zoomable><img src="https://source.unsplash.com/random/200x200?sig=4"></img></Zoomable></SwiperSlide> 
            
            
            </Swiper>
        </div>
    )}
}

