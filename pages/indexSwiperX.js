import SwipX from '../components/SwipX'
import disableScroll from 'disable-scroll';



export default function Home() {
    
  disableScroll.off();
  
  const n = 200;    
  
  return (
   <div style={{overflowX:"none"}} >
     <div >
     <h1>Food</h1>

    </div>
    {[...Array(n)].map(() => (
      <div key={n} style={{marginTop:"200px"}} >
    <SwipX  />
    
    </div>
    ))}
    </div>

  )
}

