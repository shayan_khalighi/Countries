import Country from './Country'
import style from './countrySwiper.module.css'
import React from 'react'
import axios from 'axios'
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/swiper.min.css";

export default class CountriesSwiper extends React.Component {
    state = {
        data : []
    }
    componentDidMount(){
        axios.get('https://restcountries.eu/rest/v2/all?fields=name;capital').then(res => {
         this.setState({data: res.data})
         
    })
    
      }
      render(){
    return (
        <div className={style.container}>
        <h1>Countries</h1>
        <Swiper>
        
        
            {this.state.data.map((country) => (
            
                <SwiperSlide>
                <Country country={country} />
                </SwiperSlide>
            ))}
            
            
            
            </Swiper>
        </div>
    )}
}

