import { useSpring, animated } from "react-spring";
import { useDrag, useGesture } from "react-use-gesture";
import style from "./swiperX.module.css";
import React, { useState } from "react";
import disableScroll from "disable-scroll";

export default function SwipX() {
  const [drag, setDrag] = useState(false);

  const [num, setNum] = useState(0);
  const dragEnd = () => {
    setNum(num + 1);
  };

  const [props, api] = useSpring(() => ({ x: 0 }));

  const bind = useGesture({
    onDrag: ({ down, movement: [mx] }) => {
      if (mx < 0) {
        api.start({ x: down ? mx : 0 }), { axis: "x" };
        setDrag(true);
        
      }
      
    },
    onDragEnd: ({ movement: [mx] }) => {
      if (mx < -280) dragEnd();
    
    },
  });

  function handleClick(e) {
    if (drag) {
      console.log(drag);
      
      setDrag(false);
    } 
  }

  return (
    <div>
      <div className={style.main} onClick={() => handleClick()}>
        <animated.div className={style.div} {...bind()} style={props} />
      </div>
      {num}
    </div>
  );
}
